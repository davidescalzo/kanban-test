import { O_DIRECT } from "constants";

export interface IState {
    users: IUser[];
    loading: boolean;
    stages: number[];
    cities: string[];
    names: string[];
    cityFilter?: string;
    nameFilter?: string;
}

export interface IAction {
    type: string;
    users?: IUser[];
    uuid?: string;
    message?: string;
    city?: string;
    name?: string;
}

export interface IUser {
    name: {
        first: string;
        last: string;
    };
    location: {
        city: string;
    };
    login: {
        uuid: string;
    };
    stage: number;
}

export interface IFilter {
    values: string[];
    setFilter: (value: string) => void;
}