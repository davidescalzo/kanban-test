import styled from 'styled-components';

const button = styled.div`
    display: flex;
    background: #999;
    height: 30px;
    width: 30px;
    border-radius: 15px;
    align-items: center;
    justify-content: center;
    color: white;
    font-size: 22px;
    cursor: pointer;
`;

export default button;