import styled from 'styled-components';

const Column = styled.div`
    display: flex;
    flex-direction: column;
    padding: 20px;
    margin: 20px;
    flex: 1;
`;

export default Column;