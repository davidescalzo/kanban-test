import * as React from 'react';
import styled from 'styled-components';
import { IUser } from '../types';
import Button from './Button'

const Card = styled.div`
    padding: 20px;
    border-radius: 4px;
    background: white;
    text-transform: capitalize;
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

interface IProps {
    user: IUser;
    moveBack: (uuid: String) => void;
    moveForward: (uuid: String) => void;
}

const UserCard = ({user, moveBack, moveForward}: IProps) => <Card>
    <Button onClick={() => moveBack(user.login.uuid)}>«</Button>
    <div>
        <h3>{user.name.first} {user.name.last}</h3>
        <div>{user.location.city}</div>
    </div>
    <Button onClick={() => moveForward(user.login.uuid)}>»</Button>
</Card>

export default UserCard;