import * as React from 'react';
import styled from 'styled-components';
import {IFilter} from '../types';

const FilterComp = styled.select`
    margin: 10px;
`;

const Filter = (props: IFilter) => <FilterComp onChange={(e) => props.setFilter(e.currentTarget.value)} >
    <option></option>
    {props.values.map((i, key) => <option key={key}>{i}</option>)}
</FilterComp>

export default Filter;