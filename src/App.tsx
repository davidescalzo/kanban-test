import * as React from "react";
import Kanban from './containers/Kanban';

class App extends React.Component<{}, {}> {
  render() {
    return (
      <Kanban />
    );
  }
}

export default App;
