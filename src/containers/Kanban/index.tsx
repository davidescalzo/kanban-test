import * as React from 'react';
import { connect } from 'react-redux'
import { IState } from '../../types'
import { fetchUsers, moveBack, moveForward } from './actions'
import { IUser } from '../../types'
import UserCard from '../../components/UserCard'
import Column from '../../components/Column';
import Container from '../../components/Container';
import Filter from '../../components/Filter';
import Row from '../../components/Row';
import { setCityFilter, setNameFilter } from './actions';


interface IProps {
    fetchUsers: () => void;
    users: IUser[];
    cities: string[];
    names: string[];
    moveBack: (uuid: string) => void;
    moveForward: (uuid: string) => void;
    setCityFilter: (value: string) => void;
    setNameFilter: (value: string) => void;
    cityFilter?: string;
    nameFilter?: string;
}

class Kanban extends React.Component<IProps,{}> {
    componentDidMount() {
        this.props.fetchUsers()
    }
    render() {
      const { users, cities, cityFilter, nameFilter} = this.props;
      if (!users || !cities) return null;
      const filteredUsers = this.props.users
        .filter(i => cityFilter ? i.location.city === cityFilter : true)
        .filter(i => nameFilter ? i.name.first === nameFilter : true)
      return(
        <Container>
          <Row>
            <Filter setFilter={this.props.setCityFilter} values={this.props.cities} />
            <Filter setFilter={this.props.setNameFilter} values={this.props.names} />
          </Row>
          <Row>
            {/* Each column should take title and candidates props */}
            <Column>
              <h2>Applied</h2>
              {filteredUsers.filter(i => i.stage === 0).map((i, key) => <UserCard
                  moveBack={this.props.moveBack}
                  moveForward={this.props.moveForward}
                  key={key}
                  user={i} />)}
            </Column>
            <Column>
              <h2>Interviewing</h2>
              {filteredUsers.filter(i => i.stage === 1).map((i, key) => <UserCard
                  moveBack={this.props.moveBack}
                  moveForward={this.props.moveForward}
                  key={key}
                  user={i} />)}
            </Column>
            <Column>
              <h2>Hired</h2>
              {filteredUsers.filter(i => i.stage === 2).map((i, key) => <UserCard
                  moveBack={this.props.moveBack}
                  moveForward={this.props.moveForward}
                  key={key}
                  user={i} />)}
            </Column>
          </Row>
        </Container>
      )}
}

const mapStateToProps = (state: IState) => {
    return {
      users: state.users,
      names: state.names,
      cities: state.cities,
      cityFilter: state.cityFilter,
      nameFilter: state.nameFilter
    }
  }
  
  const mapDispatchToProps = { fetchUsers, moveBack, moveForward, setCityFilter, setNameFilter }

  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Kanban)