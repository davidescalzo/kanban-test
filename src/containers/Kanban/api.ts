import { fetchUsersURL } from './constants';

export const fetchUsersCall = () => fetch(fetchUsersURL).then(res => res.json());