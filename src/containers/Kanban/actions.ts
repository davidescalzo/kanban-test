import { fetchUsersType, fetchUsersSuccessType, moveForwardType, moveBackType, setNameFilterType, setCityFilterType } from './constants'

export const fetchUsers = () => ({
    type: fetchUsersType
})

export const fetchUsersSuccess = (users: any) => ({
    type: fetchUsersSuccessType,
    users,
})

export const moveForward = (uuid: string) => ({
    type: moveForwardType,
    uuid,
})

export const moveBack = (uuid: string) => ({
    type: moveBackType,
    uuid,
})

export const setCityFilter = (city: string) => ({
    type: setCityFilterType,
    city,
})

export const setNameFilter = (name: string) => ({
    type: setNameFilterType,
    name,
})