import { put, takeLatest, call } from 'redux-saga/effects'
import { fetchUsersType, fetchUsersSuccessType } from './constants';
import { fetchUsersCall } from './api';

export function* fetchUsersSaga() {
  try {
    const {results} = yield call(fetchUsersCall);
    yield put({ type: fetchUsersSuccessType, users: results })
  } catch(err) {
    // Should handle errors better than this
    console.log(err);
  } 
}

export function* listenFetchUsers() {
  yield takeLatest(fetchUsersType, fetchUsersSaga)
}