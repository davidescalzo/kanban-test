export const fetchUsersURL =  'https://randomuser.me/api/?nat=gb&results=5';
export const fetchUsersType = 'FETCH_USERS';
export const fetchUsersSuccessType = 'FETCH_USERS_SUCCESS';
export const moveForwardType = 'MOVE_TO_NEXT_STAGE'
export const moveBackType = 'MOVE_TO_PREVIOUS_STAGE'

export const setCityFilterType = 'SET_CITY_FILTER'
export const setNameFilterType = 'SET_NAME_FILTER'