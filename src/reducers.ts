import { IState, IAction, IUser } from './types';
import { fetchUsersType, fetchUsersSuccessType, moveBackType, moveForwardType, setCityFilterType, setNameFilterType } from './containers/Kanban/constants';

export const initialState: IState = {
  users: [],
  loading: false,
  stages: [0, 1, 2], // hard coded, should be more flexible
  cities: [],
  names: [],
  cityFilter: null,
  nameFilter: null,
}

// ugly, replace with immutable.js asap
const mainReducer = (state: IState = initialState, action: IAction) => {
  let newUsers; 
  switch (action.type) {
    case fetchUsersType:
      return Object.assign({}, state, {
        loading: true
      })
    case fetchUsersSuccessType:
      const users = action.users.map((i: IUser) => {
        i.stage = 0;
        return i;
      })
      const cities = action.users.map((i: IUser) => i.location.city);
      const names = action.users.map((i: IUser) => i.name.first);
      return Object.assign({}, state, { users, loading: false, cities, names })

    case moveBackType:
      newUsers = [...state.users];
      newUsers.map(i => {
        if (i.login.uuid !== action.uuid) {
          return i;
        } else {
          i.stage = i.stage > 0 ? i.stage -= 1 : 0;
          return i;
        }
      })
      return Object.assign({}, state, {
        users: newUsers
      })

    case moveForwardType:
      newUsers = [...state.users];
      newUsers.map(i => {
        if (i.login.uuid !== action.uuid) {
          return i;
        } else {
          i.stage = i.stage < 2 ? i.stage += 1 : 2;
          return i;
        }
      })
      return Object.assign({}, state, {
        users: newUsers
      })
    case setCityFilterType:
      return Object.assign({}, state, {
        cityFilter: action.city.length > 0 ? action.city : null // the filter itself should return null
      })

      case setNameFilterType:
      return Object.assign({}, state, {
        nameFilter: action.name.length > 0 ? action.name : null // the filter itself should return null
      })
      
    default:
      return state
  }
}
export default mainReducer;