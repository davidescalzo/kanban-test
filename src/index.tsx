import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage'
import App from './App'
import './reset.css'
import './app.css';
import reducers from './reducers'
import { listenFetchUsers } from './containers/Kanban/saga';

const persistedReducer = persistReducer({
  key: 'redux-persist-storage',
  storage,

}, reducers);

const sagaMiddleware = createSagaMiddleware();

const store = createStore(persistedReducer, composeWithDevTools(
  applyMiddleware(sagaMiddleware),
));

// use library or listen to events to sync across tabs
let persistor = persistStore(store);

sagaMiddleware.run(listenFetchUsers)

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById("app"),
);