import { IState } from './types'
import reducers, {initialState} from './reducers';
import { fetchUsersType, fetchUsersSuccessType, moveForwardType, moveBackType } from './containers/Kanban/constants'


const user1 = {
    name: {
        first: 'Davide',
        last: 'Scalzo',
    },
    location: {
        city: 'London',
    },
    login: {
        uuid: '123',
    },
    stage: 0,
}

const user2 = {
    name: {
        first: 'Johnny',
        last: 'Bravo',
    },
    location: {
        city: 'Paris',
    },
    login: {
        uuid: '456',
    },
    stage: 0,
}

describe('Kanban Reducer', () => {
    let reducer: IState;
    it('should return the initial state', () => {
        reducer = reducers(undefined, {type: 'INITIAL ACTION'})
        expect(reducer).toEqual(initialState);
    });

    it('should set the state to loading when fetching', () => {
        reducer = reducers(reducer, { type: fetchUsersType })
        expect(reducer.loading).toBe(true);
    });

    it('should add the users to the state', () => {
        reducer = reducers(reducer, { type: fetchUsersSuccessType, users: [user1, user2] })
        expect(reducer.users).toEqual([user1, user2]);
        expect(reducer.loading).toBe(false);
    });

    it('should move the user to the next column', () => {
        reducer = reducers(reducer, { type: moveForwardType, uuid: '123' })
        expect(reducer.users[0].stage).toEqual(1);
        expect(reducer.users[1].stage).toEqual(0);
    });

    it('should not move the user further than the last column', () => {
        reducer = reducers(reducer, { type: moveForwardType, uuid: '123' })
        reducer = reducers(reducer, { type: moveForwardType, uuid: '123' })
        reducer = reducers(reducer, { type: moveForwardType, uuid: '123' })
        expect(reducer.users[0].stage).toEqual(2);
        expect(reducer.users[1].stage).toEqual(0);
    });

    it('should move the user to the previous column', () => {
        reducer = reducers(reducer, { type: moveBackType, uuid: '123' })
        expect(reducer.users[0].stage).toEqual(1);
        expect(reducer.users[1].stage).toEqual(0);
    });

    it('should not move the user to the previous column if in the first stage', () => {
        reducer = reducers(reducer, { type: moveBackType, uuid: '123' })
        reducer = reducers(reducer, { type: moveBackType, uuid: '123' })
        reducer = reducers(reducer, { type: moveBackType, uuid: '123' })
        expect(reducer.users[0].stage).toEqual(0);
        expect(reducer.users[1].stage).toEqual(0);
    });

});