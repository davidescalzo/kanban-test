const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  entry: "./src/index.tsx",
  output: {
    filename: "bundle.js",
    path: __dirname + "/dist"
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
  },
  module: { 
    rules: [{
      test: /\.tsx?$/,
      loader: "awesome-typescript-loader",
    }, {
      enforce: "pre",
      test: /\.js$/,
      loader: "source-map-loader",
    },
    {
      test: /\.css$/,
      use: [
        { loader: "style-loader" },
        { loader: "css-loader" }
      ]
    }
  ]},
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
};
